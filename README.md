# Flatpak theme updater

Do you want to package a theme for flathub, but it has so many variants that maintaining each of them in separate repository is a chore?

Flatpak theme updater is small python 3 script to manage multiple flatpak repositories: once configured, you only need to modify the relative template and run `theme_update.sh` to update all your repositories at once with the latest release.

***

If the script looks a bit messy, or not entirely "pythonic", it's because it's the first python script I wrote :)

Suggestions and merge requests are always welcome.