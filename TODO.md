For 1.0

- [x] make preset option in the command line case insensitive
- [x] add requirements documentation

For 1.1

- [x] archive selection from assets with regular expression
- [x] split presets in a different repository linked via submodule
- [x] configuration to allow parallel building of a preset

Other:

- [ ] support gitlab as source
- [ ] support qt themes
- [ ] automated git commit and push for each repo
- [ ] custom presets directory
- [ ] preset by directory
- [ ] list available presets
- [ ] add command line options and arguments documentation
