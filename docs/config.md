# Config.json file

Contains the preset configuration in `.json` format.

The object must have the following fields:
- "name": the name of the theme
- "source_type": indicates the format of the source. Two types are supported, and which varaibles must be specified in the relative templates depend on this.
- "archive_selector": ONLY if "source_type" is archive. Selects with a regular expression which archive to download from the release assets. Uses the default archive if empty.
- "repo": object cotaining info regarding the source repository:
  - "owner": the user owner of the repository
  - "name": the name of the repositorry
- "variants": an array of arrays, where each internal array contains the possible exclusive variants of a theme.
- "skip_variants": an array of arrays, where each internal array matches one of the possible combinations of items from the internal arrays in "variants". The combination is excluded from the building
