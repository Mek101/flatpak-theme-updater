# Templates directory

Contains the flatpak manifets and appdata templates

`manifest.template` contains the json template for the flatpak manifest. Inside it, those variables are defined:
- ${theme_name}: the base name of the theme (eg: Matcha)
- ${theme_v0} ... ${theme_vn}: the nth variant of the theme, each for every internal list in varinsts in the theme config file (eg: dark, azul)
It can then specify different tags depending on the source type (configurable in config.json)
- Archive
  - ${archive_url}: the url from which to downlaod the source tarball
  - ${archive_sha256}: the sha256 of the source tarball
- Git
  - ${repo_url}: the git repository url
  - ${release_tag}: the git tag of the release
  - ${release_commit}: the commit corresponding to the tag

`appdata.template` contains the xml template for the flathub appdata file. Inside it, those variables are defined:
- ${theme_name}: the base name of the theme (eg: Matcha)
- ${theme_v0} ... ${theme_vn}: the nth variant of the theme, each for every internal list in varinsts in the theme config file (eg: dark, azul)

`readme.template` contains the markdown template for the repository README. Inside it, this variable is defined:
- ${theme}: the complete name of the theme (eg: Matcha-dark-sea)
- ${version}: the release version
- ${release_date}: the release date
- ${release_notes}: the release description
