# Requirements for the script

This python script is obviously limited to a linux distribution.
It has been developed on `python 3.8.5`, but I tried not to use anything specific after `python 3.5`.
It's required to have on the PATH varaible:
- python3
- flatpak-builder
