# This python module handles acquiring information from the source repository
import re
import hashlib
import requests
from typing import Dict

from configure import SourceType

class BuildData():
	version: str
	release_date: str
	release_notes: str

	def get_source_data(self) -> Dict[str, str]:
		raise NotImplementedError
	def get_info(self) -> str:
		raise NotImplementedError


def __get_archive_url(data, config) -> str:
	if config.archive_selector:
		# If there's a selector, get the assets and select the correct one
		assets_list = list(requests.get(data["assets_url"]).json())

		expr = re.compile(config.archive_selector)
		for asset_data in assets_list:
			if expr.match(asset_data["name"]):
				return asset_data["browser_download_url"]
		
		raise IOError("Asset not found!")
	else:
		return data["tarball_url"]



def __get_archive_data(data, config) -> BuildData:
	archive_url = __get_archive_url(data, config)
	archive = requests.get(archive_url)

	class ArchiveData(BuildData):
		_url = archive_url
		_sha256 = hashlib.sha256(archive.content).hexdigest()

		def get_source_data(self) -> Dict[str, str]:
			return { "archive_url": self._url, "archive_sha256": self._sha256 }
		def get_info(self) -> str:
			info = "\tarchive url: {url}\n\tversion: {ver}\n\tsha256: {sha}\n\trelease notes: {notes}\n\trelease date: {date}"
			return info.format(url = self._url, ver = self.version, sha = self._sha256, notes = self.release_notes, date = self.release_date)
	
	return ArchiveData()


def __get_git_data(data, config) -> BuildData:
	middle_url = config.source_repo_owner + "/" + config.source_repo_name

	repo_url = "https://github.com/" + middle_url + ".git"
	release_tag = data["tag_name"]

	commit = requests.get("https://api.github.com/repos/" + middle_url + "/tags").json()[0]["commit"]["sha"]

	class GitData(BuildData):
		_url = repo_url
		_tag = release_tag
		_commit = commit

		def get_source_data(self) -> Dict[str, str]:
			return { "repo_url": self._url, "release_tag": self._tag, "release_commit": self._commit}
		def get_info(self) -> str:
			info =  "\trepository url: {url}\n\tversion: {ver}\n\ttag: {tag}\n\tcommit: {com}\n\trelease notes: {notes}\n\trelease date: {date}"
			return info.format(url = self._url, ver = self.version, tag = self._tag, com = self._commit)
	
	return GitData()


def get_latest_build_data(config) -> BuildData:
	data = requests.get("https://api.github.com/repos/" + config.source_repo_owner + "/" + config.source_repo_name + "/releases").json()[0]

	source = None
	if config.build_type == SourceType.ARCHIVE:
		source = __get_archive_data(data, config)
	elif config.build_type == SourceType.GIT:
		source = __get_git_data(data, config)
	
	source.version = data["tag_name"]
	source.release_date = data["published_at"][:10]
	source.release_notes = data["body"]

	return source
