import os
import subprocess
from typing import List

import utils


REPOS_DIR = "repos/"


def __git_in_path_cmd(path: str) -> List[str]:
	return [ "git", "-C", path ]


def __git_submodule_update() -> List[str]:
	return [ "git", "submodule", "update" ]


def ensure_repos(verbose: bool) -> bool:
	if not os.path.isdir(REPOS_DIR):
		if verbose:
			utils.print_info(REPOS_DIR + " is missing, creating...")
		os.mkdir(REPOS_DIR)
	elif verbose:
		utils.print_info(REPOS_DIR + " already present")
	
	# Pull git submodules
	if verbose:
		utils.print_info("pulling repos")

	cmd_init = __git_submodule_update()
	cmd_init.extend([ "--init", "--recursive" ])
	if utils.run(cmd_init, verbose).returncode != 0:
		return False

	cmd_pull = __git_submodule_update()
	cmd_pull.extend([ "--recursive", "--remote" ])
	return utils.run(cmd_pull, verbose).returncode == 0


def is_repo(path: str) -> bool:
	return os.path.isdir(path + "/.git")


def pull_repo(path: str, verbose: bool) -> bool:
	cmd = __git_in_path_cmd(path)
	cmd.append("pull")

	if verbose:
		utils.print_info("Updating repo...")
	return utils.run(cmd, verbose).returncode == 0


def commit_repo(path: str, message: str, verbose: bool) -> bool:
	cmd_add = __git_in_path_cmd(path)
	cmd_add.extend(["add", "--all"])
	if utils.run(cmd_add, verbose).returncode != 0:
		return False

	cmd_commit = __git_in_path_cmd(path)
	cmd_commit.extend(["commit", "-am", message])

	if verbose:
		utils.print_info("Committing repo '" + path + "' with message '" + message + "'")
	else:
		utils.print_info("Committing repo...")
	return utils.run(cmd_commit, verbose).returncode == 0
