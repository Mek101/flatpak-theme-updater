import sys
import subprocess
from subprocess import CompletedProcess
from typing import List


def print_err(str: str):
	print("\033[91mERROR\033[0m:", str)


def print_warn(str: str):
	print("\033[93mWARNING\033[0m:", str)


def print_info(str: str):
	print("\033[94mINFO\033[0m:", str)


def run(cmd: List[str], print_output: bool) -> CompletedProcess:
	if print_output:
		return subprocess.run(cmd, shell=False, stdin=sys.stdin, stdout=sys.stdout, stderr=sys.stderr)
	else:
		return subprocess.run(cmd, shell=False, stdin=subprocess.DEVNULL, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)