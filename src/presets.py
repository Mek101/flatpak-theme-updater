# This module contains teh functionalities to list and select the presets
import os
from typing import Union, List

PRESET_DIR = "presets/"

def list_presets() -> List[str]:
	if os.path.isdir(PRESET_DIR):
		return os.listdir(PRESET_DIR)
	else:
		return []

def get_preset_dir_of(name: str) -> Union[str, None]:
	name = name.lower()
	for preset in list_presets():
		if preset.lower() == name:
			return PRESET_DIR + preset + "/"
	return None
