# This python module provides configuration to the application
import os
import json
import enum
import itertools
from enum import Enum, IntEnum
from typing import Dict, List, Tuple, Union
from argparse import ArgumentParser

import presets


def __to_pair(i: int, variant: str) -> Tuple[str, str]:
	return ("theme_v" + str(i), variant)


def __varaint_tuple_to_dict(t: Tuple) -> Dict[str, str]:
	return dict(__to_pair(i, "-" + v) if v != "" else __to_pair(i, "") for i, v in enumerate(t))


def __map_variants(config_json):
	skip = set(map(tuple, config_json["skip_variants"]))
	all_variants = set(map(tuple, itertools.product(*config_json["variants"])))
	selected_variants = all_variants.difference(skip)

	return map(__varaint_tuple_to_dict, selected_variants)


def __override_dict(override) -> Dict[str, List[Union[str, bool]]]:
	if override:
		return { pair[0]: [ pair[1], False ] for pair in override }
	else:
		return {}


def __get_args():
	parser = ArgumentParser()
	parser.add_argument("preset", help="preset in " + presets.PRESET_DIR + " to use")
	parser.add_argument("-v", "--verbose", help="increase output verbosisty", action="store_true")
	parser.add_argument("-f", "--fail-fast", help="exit with an error code at the first failed build", action="store_true")
	parser.add_argument("-p", "--parallelize", help="build multiple repositories at the same time", action="store_true")
	parser.add_argument("-i", "--init-repos", help="initialize the repo folders if absent", action="store_true")
	parser.add_argument("-c", "--commit", help="commit the resulting repo with message. Defaults to release notes", nargs="?", const=CommitMessage.NOTES, default=CommitMessage.NONE)
	parser.add_argument("-y", "--yes", help="skip the building data confirm", action="store_true")
	parser.add_argument("-o", "--override", help="overrides a pair of a template variable and a value", nargs=2, action="append")
	return parser.parse_args()


class CommitMessage(Enum):
	NONE = enum.auto(),
	NOTES = enum.auto(),
	MESSAGE = enum.auto()

	msg = None

	@classmethod
	def get_enum(cls, val):
		if isinstance(val, CommitMessage):
			return val
		elif isinstance(val, str):
			cmsg = CommitMessage.MESSAGE
			cmsg.msg = val
			return cmsg
		else:
			raise TypeError

	def __init__(self, msg: str):
		self.msg = msg
	
	def get_message(self):
		return self.msg



class SourceType(Enum):
	ARCHIVE = "archive"
	GIT = "git"

	@classmethod
	def get_enum(cls, val: str):
		for member in cls:
			if member.value == val:
				return member
		raise Exception("Unknow source type `" + val + "'")


def get_config_from_args():
	args = __get_args()

	preset = args.preset
	preset_dir = presets.get_preset_dir_of(preset)

	if (preset_dir is None):
		raise IOError("Couldn't find preset directory '" + preset + "'")
	else:
		with open(preset_dir + "config.json", "r") as config_file:
			config_json = json.load(config_file)

			override_dictionary = __override_dict(args.override)

			source_type = SourceType.get_enum(config_json["source_type"])

			archive_expr = ""
			if source_type == SourceType.ARCHIVE:
				archive_expr = config_json["archive_selector"]

			mapped_variants = __map_variants(config_json)

			class Config():
				verbose = args.verbose
				skip_prompt = args.yes
				init_repos = args.init_repos
				fail_fast = args.fail_fast
				parallel = args.parallelize
				commit = CommitMessage.get_enum(args.commit)
				override_dict = override_dictionary
				templates_dir = preset_dir + "templates/"
				defaults_dir = preset_dir + "defaults/"
				build_type = source_type
				archive_selector = archive_expr
				variants = mapped_variants
				theme_name = config_json["name"]
				source_repo_owner = config_json["repo"]["owner"]
				source_repo_name = config_json["repo"]["name"]

			return Config()
