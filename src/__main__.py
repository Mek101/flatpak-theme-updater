import os
import shutil
import itertools
from typing import Dict, List, Union
from distutils import util
from string import Template
from types import SimpleNamespace
from argparse import ArgumentParser
from concurrent.futures import ThreadPoolExecutor

import utils
import repos
import source
import configure
from source import BuildData
from configure import CommitMessage


CONFIG = None


def render_template(template_path: str, args: Dict[str, str]) -> str:
	with open(template_path, "r") as content_file:
		content = content_file.read()
		return Template(content).safe_substitute(**args)


def variant_str(variant: Dict[str, str]) -> str:
	return "".join([var for var in variant.values() if var != ""])


def get_flatpak_name(variant: Dict[str, str]) -> str:
	return "org.gtk.Gtk3theme." + CONFIG.theme_name + variant_str(variant)


def try_override(args: Dict[str, str], override_dict: Dict[str, List[Union[str, bool]]]) -> Dict[str, str]:
	if override_dict:
		for arg in args:
			if arg in override_dict:
				args[arg] = override_dict[arg][0]
				override_dict[arg][1] = True

	return args


def override_any_unused(override_dict: Dict[str, List[Union[str, bool]]]) -> Dict[str, str]:
	return { arg: override for arg, override in override_dict.items() if not override[1] }


def apply_appdata_template(template_dir: str, repo_dir: str, name: str, override_dict: Dict[str, List[Union[str, bool]]], theme_variant: Dict[str, str], version: str, release_date: str, release_notes: str) -> None:
	if CONFIG.verbose:
		utils.print_info("building appdata for " + name)
	
	args = {"theme_name": CONFIG.theme_name, "version": version, "release_date": release_date, "release_notes": release_notes, **theme_variant}
	args = try_override(args, override_dict)
	appdata_content = render_template(template_dir + "appdata.template", args)
	with open(repo_dir + name + ".appdata.xml", "w+") as out_file:
		out_file.write(appdata_content)


def apply_manifest_template(template_dir: str, repo_dir: str, name: str, override_dict: Dict[str, List[Union[str, bool]]], theme_variant: Dict[str, str], source_data: Dict[str, str]) -> None:
	if CONFIG.verbose:
		utils.print_info("building manifest for " + name)
	
	args = {"theme_name": CONFIG.theme_name, **theme_variant, **source_data}
	args = try_override(args, override_dict)
	manifest_content = render_template(template_dir + "manifest.template", args)
	with open(repo_dir + name + ".json", "w+") as out_file:
		out_file.write(manifest_content)


def apply_defaults(defaults_dir: str, repo_dir: str, name: str) -> None:
	if CONFIG.verbose:
		utils.print_info("copying defaults for " + name)
	shutil.copytree(defaults_dir, repo_dir, dirs_exist_ok = True)


def test_flatpak_build(repo_dir: str, name: str) -> bool:
	if CONFIG.verbose:
		utils.print_info("trying to build flatpak...")

	cmd = [
			"flatpak-builder",
			"--force-clean",
			"--state-dir=" + repo_dir + ".flatpak-builder",
			repo_dir + "build-dir",
			repo_dir + name + ".json"
		]

	res = utils.run(cmd, CONFIG.verbose)

	if res.returncode == 0:
		utils.print_info("build of '" + name + "' succeded!")
		return True
	else:
		utils.print_err("test building of '" + name + "' failed!")
		return False


def apply_for_variant(variant: Dict[str, str], build_data: BuildData) -> None:
	name = get_flatpak_name(variant)
	repo_dir = "repos/" + name + "/"

	if (not os.path.isdir(repo_dir)):
		if (CONFIG.init_repos):
			if CONFIG.verbose:
				utils.print_info("initializing '" + name + "' directory")
			os.makedirs(repo_dir, exist_ok = True)
		else:
			utils.print_warn("skipping '" + name + "': repository not found")
			return
	elif repos.is_repo(repo_dir): # Check if the repo directory actually contains a .git repository
		repos.pull_repo(repo_dir, CONFIG.verbose)


	if not CONFIG.verbose:
		utils.print_info("building templates and testing '" + name + "'")

	apply_appdata_template(CONFIG.templates_dir, repo_dir, name, CONFIG.override_dict, variant, build_data.version, build_data.release_date, build_data.release_notes)
	apply_manifest_template(CONFIG.templates_dir, repo_dir, name, CONFIG.override_dict, variant, build_data.get_source_data())

	unused = override_any_unused(CONFIG.override_dict)
	if unused:
		utils.print_warn("unused overrides: " + str(unused))

	apply_defaults(CONFIG.defaults_dir, repo_dir, name)

	if not test_flatpak_build(repo_dir, name) and CONFIG.fail_fast:
		exit(-1)
	
	if CONFIG.commit == CommitMessage.NOTES:
		repos.commit_repo(repo_dir, build_data.release_notes, CONFIG.verbose)
	elif CONFIG.commit == CommitMessage.MESSAGE:
		repos.commit_repo(repo_dir, CONFIG.commit.get_message(), CONFIG.verbose)


CONFIG = configure.get_config_from_args()

print("Getting latest version from 'https://github.com/" + CONFIG.source_repo_owner + "/" + CONFIG.source_repo_name + "'...")
build_data = source.get_latest_build_data(CONFIG)

print("Building with:\n" + build_data.get_info())
if not CONFIG.skip_prompt:
	# Display conform prompt unless configured to skip it
	try:
		if not util.strtobool(input("Proceed? [y/N] ")):
			exit(0)
	except ValueError:
		exit(0)

# Ensures the repository directory exists
if not repos.ensure_repos(CONFIG.verbose):
	utils.print_err("the repository aren't in a commit-compatible state. Re run with '-v' for more information")
	exit(-1)

if CONFIG.parallel:
	with ThreadPoolExecutor() as executor:
		executor.map(apply_for_variant, CONFIG.variants, itertools.repeat(build_data))
else:
	for variant in CONFIG.variants:
		apply_for_variant(variant, build_data)
